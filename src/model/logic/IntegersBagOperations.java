package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {

	
	
	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	
	
	public int getMax(IntegersBag bag){
		int max = Integer.MAX_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}
			
		}
		return max;
	}
	
	public int getMin(IntegersBag bag) {
		int min = Integer.MIN_VALUE;
		int value;
		if (bag != null) {
			Iterator<Integer> iter = bag.getIterator();
			while (iter.hasNext()) {
				value = iter.next();
				if (min > value) {
					min = value;
				}
			}
		}
		return min;
	}

	public int darModa(IntegersBag bag) {
		int max = 0;
		int moda = 0;
		if (bag != null) {
			Iterator<Integer> iter = bag.getIterator();
			while (iter.hasNext()) {
				int repeticiones = 0;
				Iterator<Integer> iter1 = bag.getIterator();
				while (iter1.hasNext()) {
					if (iter.next() == iter1.next()) {
						repeticiones++;
					}
				}
				if (repeticiones > max) {
					moda = iter.next();
					max = repeticiones;
				}
			}
		}
		return moda;
	}

	public int sumatoria(IntegersBag bag) {
		int suma = 0;
		if (bag != null) {
			Iterator<Integer> iter = bag.getIterator();
			while (iter.hasNext()) {
				int numA = iter.next();
				Iterator<Integer> iter1 = bag.getIterator();
				while (iter1.hasNext()) {
					int numB = iter1.next();
					suma = numA + numB;
				}
			}
		}
		return suma;
	}
}
